#!/usr/bin/env python3.9
# vim: set ai et ff=unix sts=4 sw=4 ts=4 :
# -*- coding: utf-8 -*-

############################################################
# Package: Python Docker Test
# URL: https://gitlab.com/k.shtrykov/docker-test
# Author: Kirill Shtrykov <kirill@shtrykov.com>
# Website: https://shtrykov.com
############################################################

import os
import sys
import logging
import uvicorn
from fastapi import FastAPI, HTTPException
from starlette.responses import FileResponse, Response
from starlette_prometheus import metrics, PrometheusMiddleware

# Get version from VERSION file
with open(os.path.join(getattr(sys, '_MEIPASS', '.'), 'VERSION'), 'r') as fh:
    __version__ = fh.read().strip()

STATIC_PATH = os.path.join(getattr(sys, '_MEIPASS', '.'), 'static')
FAVICON_FILE = 'favicon.ico'
ACCEPTABLE_CODES = {
    200: {
        'content': 'OK'
    },
    400: {
        'content': 'Bad Request'
    },
    404: {
        'content': 'Not Found'
    },
    500: {
        'content': 'Internal Server Error'
    },
    503: {
        'content': 'Service Unavailable'
    },
    504: {
        'content': 'Gateway Timeout'
    }
}

app = FastAPI()
app.add_middleware(PrometheusMiddleware)
app.add_route('/metrics', metrics)


@app.get('/favicon.ico')
async def favicon():
    path = os.path.join(STATIC_PATH, FAVICON_FILE)
    return FileResponse(path)


@app.get('/')
def main():
    return 'OK'


@app.get('/version')
def version():
    return f'Version: {__version__}'


@app.get('/log')
def log():
    generate_logentries()


@app.get('/response/{code}')
def response(code: int):
    if code not in ACCEPTABLE_CODES:
        raise HTTPException(200, f"not acceptable code: {code} ({','.join([str(code) for code in ACCEPTABLE_CODES])})")
    return Response(status_code=code, content=ACCEPTABLE_CODES[code].get('content'))


def generate_logentries():
    logging.info('First log line')
    logging.info('Second log line')
    logging.info('Third log line')


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8080)
