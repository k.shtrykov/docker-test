# Docker Test

## 0.1.2
- feat: Rewrite to FastAPI
- feat: Add `/response/{code}` endpoint
- feat: Add `favicon.ico`
- ci: Pump `Alpine Linux` image version
- docs: Update `README.md`

## 0.1.1
- feat: Add `/varsion` endpoint
- feat: Add `/log` endpoint

## 0.1.0
Initial release
- docs: Update `README.md`
- docs: Add `CHANGELOG.md`
- feat: Add `/` location with `200 OK` response
- feat: Add `/metrics` location with Prometheus Flask Exporter metrics
