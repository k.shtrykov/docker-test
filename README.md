# Docker Test
Yet another docker image for tests

## Running
Start container binding the external port `8080`.
```commandline
docker run -d --name=docker-test -p 8000:8000 registry.gitlab.com/k.shtrykov/docker-test
```

## Locations
- `/` - root location; returns 200 OK
- `/metrics` - Prometheus Flask Exporter location; returns Prometheus metrics
- `/respone/{code}` - returns page with HTTP status code specified as `{code}`;
for example `/response/404` returns `HTTP[404] Not Found` page; Acceptable codes:
200, 400, 404, 500, 503, 504
