#!/usr/bin/env python3.9

import main
from fastapi.testclient import TestClient

client = TestClient(main.app)


def test_main():
    response = client.get('/')
    assert response.status_code == 200
    assert response.text == '"OK"'


def test_metrics():
    response = client.get('/metrics')
    assert response.status_code == 200


def test_version():
    response = client.get('/version')
    assert response.status_code == 200
    assert response.text == f'"Version: {main.__version__}"'


def test_response():
    for code in main.ACCEPTABLE_CODES:
        response = client.get(f'/response/{code}')
        assert response.status_code == code
        assert response.text == main.ACCEPTABLE_CODES[code].get('content')
    response = client.get(f'/response/700')
    assert response.status_code == 200
