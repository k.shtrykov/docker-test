FROM python:3.9-alpine3.14 AS builder

RUN apk add --update \
    binutils git gcc musl-dev libc-dev libffi-dev zlib-dev && \
    # Build bootloader for alpine
    git clone https://github.com/pyinstaller/pyinstaller.git /tmp/pyinstaller \
    && cd /tmp/pyinstaller/bootloader \
    && CFLAGS="-Wno-stringop-overflow" python ./waf configure --no-lsb all \
    && pip install .. \
    && rm -Rf /tmp/pyinstaller

COPY . /tmp/docker-test

WORKDIR /tmp/docker-test

RUN pip3 install -r requirements.txt && \
    pyinstaller \
    --noconfirm \
    --onefile \
    --log-level DEBUG \
    --clean \
    --add-data VERSION:. \
    --add-data static:static \
    main.py

FROM alpine:3.14

COPY --from=builder /tmp/docker-test/dist/main /bin/main

RUN chown 0:0 /bin/main && \
    chmod a+x /bin/main

EXPOSE 8080

USER nobody

ENTRYPOINT  [ "/bin/main" ]
